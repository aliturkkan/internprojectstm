from django.shortcuts import render, HttpResponse
from zipfile import ZipFile

from os import listdir
from requests import get
from matplotlib.pyplot import xlabel, ylabel, bar, title, xticks, legend, figure, plot
from matplotlib.style import use
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from xml.etree.ElementTree import ElementTree


def NVD_show_table(request):

    #yıllar ve her yılda kayıt alınan rapor sayılarını tutan listeler
    sum_vulnerabilities = []
    years = []

    for i in range(2002, 2019):

        try:
            #NVDxml dosyası içindeki her xml dosyasında bulunan
            #güvenlik açıklarını counter değişkenine at
            tree = ElementTree(file="NVDxml/nvdcve-2.0-" + str(i) + ".xml")
            root = tree.getroot()
            counter = 0
            for child in root:
                counter = counter + 1

        except Exception as e:
            print(str(e))

        sum_vulnerabilities.append(counter)
        years.append(i)

    #Elde edilen verileri grafiğe aktar
    fig = figure(figsize=(10, 5))
    use('ggplot')
    bar(years, sum_vulnerabilities, color="r", align="center", width=0.5, label="NVD Verileri")
    title("NVD Verilerinden Alınan ve Raporlanan Güvenlik Açıklarının\n Yıllara Göre Dağılımı")
    xticks(years)
    xlabel("Yıllar", color=(0, 0, 0))
    ylabel("Raporlanan Güvenlik Açığı Sayısı", color=(0, 0, 0))
    legend()

    #Oluşan grafiği resim haline getirerek return ile gönder
    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

#Veriler sık sık güncellendiği için NVD sitesinden
#verileri çekip NVDxml dosyadaki veriler ile değiştiren fonksiyon
def update_NVD(request):

    '''
    get('https://nvd.nist.gov/vuln/data-feeds#XML_FEED')

    for i in range(2002, 2019):
        url = "nvdcve-2.0-" + str(i) + ".xml.zip"

        r_file = get("https://static.nvd.nist.gov/feeds/xml/cve/2.0/" + url, stream=True)
        with open(url, 'w+b') as f:
            for chunk in r_file:
                f.write(chunk)

        #İndirilen dosyalar zip uzantılı geldiği için dosyaları zip'den çıkart
        zip_ref = ZipFile('C:/InternProjectSTM/'+url, 'r')
        zip_ref.extractall('C:/InternProjectSTM/NVDxml')
        zip_ref.close()
    '''


#NVD_show_table fonksiyonu ile aynı işlevi yapar
#Bu işlemleri CNNVD verileri için tekrarlar
def CNNVD_show_table(request):
    sum_vulnerabilities = []
    years = []

    for i in range(2015, 2019):

        #klasördeki xml dosyalarının sayıları eşit olmadığı için ilk olarak
        # dizindeki dosaya sayısını bul ve for döngüsünü bu sayı kadar döndür
        number_of_files = listdir("CNNVDxml/CNNVD_XML_"+str(i))
        counter = 0
        for j in range(1, len(number_of_files)+1):
            try:
                tree = ElementTree(file="CNNVDxml/CNNVD_XML_"+str(i)+"/1 (" + str(j) + ").xml")
                root = tree.getroot()

                for child in root:
                    for child2 in child:
                        for child3 in child2.findall('cve'):
                            counter = counter + 1

            except Exception as e:
                print(str(e))

        sum_vulnerabilities.append(counter)
        years.append(i)

    fig = figure(figsize=(10, 5))
    use('ggplot')
    bar(years, sum_vulnerabilities, color="b", align="center", width=0.5, label="CNNVD Verileri")
    title("CNNVD Verilerinden Alınan ve Raporlanan Güvenlik Açıklarının\n Yıllara Göre Dağılımı")
    xticks(years)
    xlabel("Yıllar", color=(0, 0, 0))
    ylabel("Raporlanan Güvenlik Açığı Sayısı", color=(0, 0, 0))
    legend()

    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

#NVD_show_table ve CNNVD_show_table fonksiyonlarını birleşimidir
#Amamç her iki veriyi aynı grafikte görebilmek
def NVDvsCNNVD_show_table(request):
    NVD_sum_vulnerabilities = []
    NVD_years = []
    CNNVD_sum_vulnerabilities = []

    for i in range(2015, 2019):

        # NVD Verileri
        try:
            NVD_tree = ElementTree(file="NVDxml/nvdcve-2.0-" + str(i) + ".xml")
            NVD_root = NVD_tree.getroot()
            NVD_counter = 0
            for child in NVD_root:
                NVD_counter = NVD_counter + 1

        except Exception as e:
            print(str(e))

        # CNNVD Verileri
        number_of_files = listdir("CNNVDxml/CNNVD_XML_" + str(i))
        CNNVD_counter = 0
        for j in range(1, len(number_of_files)+1):
            try:
                CNNVD_tree = ElementTree(file="CNNVDxml/CNNVD_XML_"+str(i)+"/1 (" + str(j) + ").xml")
                CNNVD_root = CNNVD_tree.getroot()

                for child in CNNVD_root:
                    for child2 in child:
                        for child3 in child2.findall('cve'):
                            CNNVD_counter = CNNVD_counter + 1

            except Exception as e:
                print(str(e))

        CNNVD_sum_vulnerabilities.append(CNNVD_counter)
        NVD_sum_vulnerabilities.append(NVD_counter)
        NVD_years.append(i)

    fig = figure(figsize=(10, 5))
    use('ggplot')
    plot(NVD_years, NVD_sum_vulnerabilities, '-ok', color="r", label="NVD Verileri")
    plot(NVD_years, CNNVD_sum_vulnerabilities, '-ok', color="b", label="CNNVD Verileri")
    title("NVD ve CNNVD Verilerinden Alınan ve Raporlanan Güvenlik Açıklarının\n Yıllara Göre Dağılımı")
    xticks(NVD_years)
    xlabel("Yıllar", color=(0, 0, 0))
    ylabel("Raporlanan Güvenlik Açığı Sayısı", color=(0, 0, 0))
    legend()

    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

#Her iki veri kaynağı olan NVD ve CNNVD arasındaki farklar
def home_page(request):
    NVD_sum_vulnerabilities = []
    NVD_years = []
    CNNVD_sum_vulnerabilities = []
    CNNVD_except_NVD = []
    NVD_ID = []
    CNNVD_ID = []
    all_years_CNNVD_except_NVD = {}
    all_years_NVD_except_CNNVD = {}

    for i in range(2015, 2019):

        # NVD Verileri
        try:
            NVD_tree = ElementTree(file="NVDxml/nvdcve-2.0-" + str(i) + ".xml")
            NVD_root = NVD_tree.getroot()
            NVD_counter = 0
            for child in NVD_root:
                NVD_ID.append(child.get("id"))
                NVD_counter = NVD_counter + 1

        except Exception as e:
            print(str(e))

        # CNNVD Verileri
        number_of_files = listdir("CNNVDxml/CNNVD_XML_" + str(i))
        CNNVD_counter = 0
        for j in range(1, len(number_of_files)+1):
            try:
                CNNVD_tree = ElementTree(file="CNNVDxml/CNNVD_XML_" + str(i) + "/1 (" + str(j) + ").xml")
                CNNVD_root = CNNVD_tree.getroot()

                for child in CNNVD_root:
                    for child2 in child:
                        for child3 in child2.findall('cve'):
                            # print(child3.find('cveNumber').text)
                            CNNVD_ID.append(child3.find('cveNumber').text)
                            CNNVD_counter = CNNVD_counter + 1

            except Exception as e:
                print(str(e))

        CNNVD_sum_vulnerabilities.append(CNNVD_counter)
        NVD_sum_vulnerabilities.append(NVD_counter)
        NVD_years.append(i)

        #CNNVD'de olup NVD'de olmayan CVE raporları listesi
        CNNVD_except_NVD = set(CNNVD_ID) - set(NVD_ID)
        # NVD'de olup CNNVD'de olmayan CVE raporları listesi
        NVD_except_CNNVD = set(NVD_ID) - set(CNNVD_ID)

        #Anahtar kelime bir sayı olamayacağı için başına string ekle
        #Bu ayrıca html ile direk başlık olarak kullanmamıza da olanak sağlayacak
        yearString = 'year_'

        all_years_CNNVD_except_NVD[yearString + str(i)] = CNNVD_except_NVD
        all_years_NVD_except_CNNVD[yearString + str(i)] = NVD_except_CNNVD

    #Elde edilen her iki farkı bir anahtar kelime ile home.html'e gönder
    return render(request, 'home.html', {'all_years_CNNVD_except_NVD': all_years_CNNVD_except_NVD,'all_years_NVD_except_CNNVD': all_years_NVD_except_CNNVD})
